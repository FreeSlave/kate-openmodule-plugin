
Openmodule plugin is an example on how to create plugins for Kate (KDE Advanced Text Editor). It's designed to open header files from editor.
Currently it has support for C++ and D modules.

## Building from source

Find libkateinterfaces.so.4 on your system. It's usually in /usr/lib if you already have kate installed.
Make link to this file named libkateinterfaces.so so ld linker could find it.

Get [Kate source code](http://kate-editor.org/get-it/)
Then run at terminal:

    cd /path/to/cloned/repository
    export KATE_SRC_PATH=~/kde/kate # change to actual path to sources on your computer
    mkdir build
    cd build
    export QT_SELECT=4
    cmake -DQT_QMAKE_EXECUTABLE=/usr/bin/qmake-qt4 ../src/ -DCMAKE_INSTALL_PREFIX=$HOME/.kde
    make && make install # since CMAKE_INSTALL_PREFIX is set to user directory no need to start make install as root
    
Note how you need to pass *QT_QMAKE_EXECUTABLE* variable to cmake if *qmake* utility targets Qt5 instead of Qt4 on your system.
