#ifndef CLANGUAGEBOX_H
#define CLANGUAGEBOX_H

#include "languagebox.h"

class CLanguageBox : public LanguageBox
{
    Q_OBJECT
public:
    explicit CLanguageBox(QWidget *parent = 0);
    
public slots:
    virtual void autoDetect();
    
public:
    QString key() const {
        return "OpenCModule";
    }
    QStringList extensions() const {
        return QStringList() << "h" << "c" << "cpp" << "hpp";
    }
};

#endif // CLANGUAGEBOX_H
