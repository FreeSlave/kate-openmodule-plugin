#ifndef LANGUAGEBOX_H
#define LANGUAGEBOX_H

#include <QGroupBox>
#include <QStringList>

class QListWidget;
class QBoxLayout;

class KPushButton;

class LanguageBox : public QGroupBox
{
    Q_OBJECT
public:
    explicit LanguageBox(QString title, QWidget *parent = 0);

public slots:
    void newLine();
    void removeLines();
    virtual void autoDetect() = 0;
    void addLine(QString str);
    void addLines(QStringList strList);

public:
    QBoxLayout* buttonLayout() {
        return buttonBox;
    }

    QStringList paths() const;
    virtual QString key() const = 0;
    virtual QStringList extensions() const = 0;

private:
    QListWidget* listWidget;
    KPushButton* buttonNew;
    KPushButton* buttonRemove;
    KPushButton* buttonAuto;

    QBoxLayout* buttonBox;
};

#endif // LANGUAGEBOX_H
