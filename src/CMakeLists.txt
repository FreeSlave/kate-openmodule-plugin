find_package(KDE4 REQUIRED)
include (KDE4Defaults)

include_directories (${CMAKE_CURRENT_BINARY_DIR} ${KDE4_INCLUDES})

include_directories ($ENV{KATE_SRC_PATH}/kate/interfaces)

set(kateopenmoduleplugin_PART_SRCS openmodule.cpp languagebox.cpp dlanguagebox.cpp clanguagebox.cpp pathchooser.cpp)

kde4_add_plugin(kateopenmoduleplugin ${kateopenmoduleplugin_PART_SRCS})

target_link_libraries(kateopenmoduleplugin ${KDE4_KDECORE_LIBS} ${KDE4_KDEUI_LIBS} ${KDE4_KTEXTEDITOR_LIBS} kateinterfaces )

########### install files ###############
install(TARGETS kateopenmoduleplugin  DESTINATION ${PLUGIN_INSTALL_DIR} )
install( FILES ui.rc  DESTINATION  ${DATA_INSTALL_DIR}/kate/plugins/kateopenmodule )
install( FILES kateopenmodule.desktop  DESTINATION  ${SERVICES_INSTALL_DIR} )

