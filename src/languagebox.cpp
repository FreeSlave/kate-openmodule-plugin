#include "languagebox.h"
#include "languagebox.moc"

#include <QBoxLayout>
#include <QFileDialog>
#include <QListWidget>
#include <QListWidgetItem>
#include <QVBoxLayout>

#include <KIcon>
#include <KPushButton>

LanguageBox::LanguageBox(QString title, QWidget *parent) :
    QGroupBox(title, parent)
{
    listWidget = new QListWidget;
    listWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);

    buttonNew = new KPushButton(KIcon("list-add"), "New");
    buttonNew->setToolTip("Add new path");
    connect(buttonNew, SIGNAL(clicked()), SLOT(newLine()));
    buttonRemove = new KPushButton(KIcon("edit-delete"), "Remove");
    buttonRemove->setToolTip("Remove selected paths");
    connect(buttonRemove, SIGNAL(clicked()), SLOT(removeLines()));
    buttonAuto = new KPushButton("Auto-detect");
    buttonAuto->setToolTip("Try to auto-detect paths");
    connect(buttonAuto, SIGNAL(clicked()), SLOT(autoDetect()));

    buttonBox = new QBoxLayout(QBoxLayout::LeftToRight);
    buttonBox->addWidget(buttonNew);
    buttonBox->addWidget(buttonRemove);
    buttonBox->addWidget(buttonAuto);
    buttonBox->addStretch();

    QVBoxLayout* vbox = new QVBoxLayout;
    vbox->addWidget(listWidget);
    vbox->addItem(buttonBox);

    setLayout(vbox);
}


void LanguageBox::newLine()
{
    QString dirName = QFileDialog::getExistingDirectory(this, "Choose directory", QDir::homePath());
    if (!dirName.isEmpty())
        addLine(dirName);
}

void LanguageBox::removeLines()
{
    foreach(QListWidgetItem* item, listWidget->selectedItems())
    {
        listWidget->takeItem(listWidget->row(item));
        delete item;
    }
}

void LanguageBox::addLine(QString str)
{
    for (int i=0; i<listWidget->count(); ++i)
    {
        QListWidgetItem* item = listWidget->item(i);
        if (item->text() == str)
            return;
    }
    listWidget->addItem(str);
}

void LanguageBox::addLines(QStringList strList)
{
    foreach(QString str, strList)
    {
        addLine(str);
    }
}

QStringList LanguageBox::paths() const
{
    QStringList stringList;
    for (int i=0; i<listWidget->count(); ++i)
    {
        stringList << listWidget->item(i)->text();
    }
    return stringList;
}
