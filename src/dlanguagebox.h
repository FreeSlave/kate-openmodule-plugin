#ifndef DLANGUAGEBOX_H
#define DLANGUAGEBOX_H

#include "languagebox.h"

class DLanguageBox : public LanguageBox
{
    Q_OBJECT
public:
    explicit DLanguageBox(QWidget *parent = 0);

public slots:
    virtual void autoDetect();
    
public:
    QString key() const {
        return "OpenDModule";
    }
    QStringList extensions() const {
        return QStringList() << "d" << "di";
    }
};

#endif // DLANGUAGEBOX_H
