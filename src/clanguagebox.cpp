#include "clanguagebox.h"
#include "clanguagebox.moc"

#include <QSet>
#include <QDir>
#include <QtAlgorithms>

CLanguageBox::CLanguageBox(QWidget *parent) :
LanguageBox("C/C++ programming Language", parent)
{
}

void CLanguageBox::autoDetect()
{   
    QSet<QString> pathSet;
    
    QStringList prefixList;
    prefixList << "/usr/include" << "/usr/local/include";
    
    QRegExp regex("\\d\\.\\d"); //for c++ versions
    
    foreach(QString prefix, prefixList)
    {
        pathSet.insert(prefix);
        
        QDir includeDir(prefix);
        if (includeDir.cd("c++"))
        {
            QStringList dirList = includeDir.entryList(QDir::Dirs|QDir::NoSymLinks|QDir::NoDotAndDotDot|QDir::Readable);
            foreach(QString dirName, dirList)
            {
                if (regex.exactMatch(dirName))
                {
                    pathSet.insert(includeDir.absolutePath() + QDir::separator() + dirName);
                }
            }
        }
    }
    
    QStringList pathList = pathSet.toList();
    qSort(pathList);
    addLines(pathList);
}
