#include "pathchooser.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QListWidget>
#include <QListWidgetItem>
#include <QVBoxLayout>

#include <KPushButton>
#include <KIcon>

PathChooser::PathChooser(QStringList lines, QWidget *parent) :
    QDialog(parent)
{
    QListWidget* listWidget = new QListWidget;
    foreach(QString line, lines)
    {
        listWidget->addItem(line);
    }
    connect(listWidget, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)), SLOT(setChosen(QListWidgetItem*)));

    KPushButton* ok = new KPushButton(KIcon("dialog-ok"), "OK");
    connect(ok, SIGNAL(clicked()), SLOT(accept()));
    KPushButton* cancel = new KPushButton(KIcon("dialog-cancel"), "Cancel");
    connect(cancel, SIGNAL(clicked()), SLOT(reject()));

    QHBoxLayout* hbox = new QHBoxLayout;
    hbox->addStretch();
    hbox->addWidget(ok);
    hbox->addWidget(cancel);

    QLabel* label = new QLabel("Resolved file name is ambiguous. Please, make your choice.");
    QVBoxLayout* vbox = new QVBoxLayout;
    vbox->addWidget(label);
    vbox->addWidget(listWidget);
    vbox->addItem(hbox);
    setLayout(vbox);
}

void PathChooser::setChosen(QListWidgetItem *item)
{
    chosen = item->text();
}
