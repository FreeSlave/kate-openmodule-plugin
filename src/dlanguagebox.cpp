#include "dlanguagebox.h"
#include "dlanguagebox.moc"

#include <QSet>
#include <QDir>
#include <QtAlgorithms>

DLanguageBox::DLanguageBox(QWidget *parent) :
    LanguageBox("D programming Language", parent)
{
}

void DLanguageBox::autoDetect()
{
    QSet<QString> pathSet;

    QStringList prefixList;
    prefixList << "/usr/include" << "/usr/local/include";

    foreach(QString prefix, prefixList)
    {
        const QDir includeDir(prefix);
        if (includeDir.isReadable())
        {
            //check for dmd
            QDir dmdDir = includeDir;
            if (dmdDir.cd("dmd"))
            {
                QDir phobosDir = dmdDir;
                if (phobosDir.cd("phobos"))
                {
                    pathSet.insert(phobosDir.absolutePath());
                }
                QDir druntimeDir = dmdDir;
                if (druntimeDir.cd("druntime/import"))
                {
                    pathSet.insert(druntimeDir.absolutePath());
                }
            }

            //check for d and d2
            QStringList dList;
            dList << "d" << "d2";

            QRegExp regex("\\d\\.\\d"); //for gdc versions

            foreach(QString dStr, dList)
            {
                QDir dDir = includeDir;
                if (dDir.cd(dStr))
                {
                    pathSet.insert(dDir.absolutePath());
                    QStringList dirList = dDir.entryList(QDir::Dirs|QDir::NoSymLinks|QDir::NoDotAndDotDot|QDir::Readable);

                    foreach(QString dirName, dirList)
                    {
                        if (regex.exactMatch(dirName))
                        {
                            pathSet.insert(dDir.absolutePath() + QDir::separator() + dirName);
                        }
                    }
                }
            }
        }
    }

    QStringList pathList = pathSet.toList();
    qSort(pathList);
    
    addLines(pathList);
}
