
#include "openmodule.h"
#include "openmodule.moc"

#include <kate/application.h>
#include <kate/mainwindow.h>

#include <ktexteditor/document.h>
#include <ktexteditor/view.h>

#include <KAboutData>
#include <KAction>
#include <KActionCollection>
#include <KConfig>
#include <KUrl>
#include <KPluginFactory>
#include <KPluginLoader>

#include <QDir>
#include <QFileInfo>
#include <QList>
#include <QMessageBox>
#include <QRegExp>
#include <QVBoxLayout>

#include "clanguagebox.h"
#include "dlanguagebox.h"
#include "pathchooser.h"

K_PLUGIN_FACTORY(KatePluginOpenModuleFactory, registerPlugin<KatePluginOpenModule>();)
K_EXPORT_PLUGIN(KatePluginOpenModuleFactory(KAboutData("kateopenmodule", "kateopenmoduleplugin", ki18n("Open Module"), "0.1", ki18n("Opens module file depending on current language"))))

//KatePluginOpenModule

KatePluginOpenModule::KatePluginOpenModule(QObject* parent, const QList<QVariant>& ) 
    : Kate::Plugin((Kate::Application *)parent, "kate-open-module-plugin")
{
    applyConfig();
}

Kate::PluginView* KatePluginOpenModule::createView(Kate::MainWindow* mainWin)
{
    KatePluginOpenModuleView* view = new KatePluginOpenModuleView(mainWin);
    connect(view, SIGNAL(openModuleRequested()), SLOT(openModuleSlot()));
    return view;
}

Kate::PluginConfigPage *KatePluginOpenModule::configPage (uint , QWidget *w, const char *name)
{
    KatePluginOpenModuleConfigPage* page = new KatePluginOpenModuleConfigPage(w, name);
    connect(page, SIGNAL(applied()), SLOT(applyConfig()));
    return page;
}

void KatePluginOpenModule::applyConfig()
{
    KConfigGroup config(KGlobal::config(), "PluginOpenModule");
    QStringList keys = config.readEntry("OpenModules", QStringList());
    foreach(QString key, keys)
    {
        QStringList paths = config.readEntry(key, QStringList());
        QStringList extensions = config.readEntry(key + "Extensions", QStringList());
        
        foreach(QString extension, extensions)
        {
            extensionToPaths[extension] = paths;
            extensionToExtensions[extension] = extensions;
        }
    }
}

void KatePluginOpenModule::openModuleSlot()
{
    KTextEditor::View* view = application()->activeMainWindow()->activeView();
    if (!view)
        return;
    
    KTextEditor::Cursor cursor = view->cursorPosition();
    if (!cursor.isValid())
        return;
    
    KTextEditor::Document* document = view->document();
    if (!document)
        return;
    
    QString line = document->line(cursor.line());
    KUrl url = document->url();
    if (!url.isValid() || url.isEmpty())
        return;
    
    QFileInfo info( url.toLocalFile() );
    QString extension = info.suffix().toLower();
    
    QString fileName;
    QStringList extensionsToTry;
    QStringList pathsToTry = extensionToPaths[extension];
    
    if (extension == "d" || extension == "di")
    {
        fileName = dModuleToFileName(line, cursor);
        extensionsToTry = extensionToExtensions[extension];
    }
    else if (extension == "c" || extension == "h" || extension == "cpp" || extension == "hpp")
    {
        fileName = cModuleToFileName(line, cursor);
        extensionsToTry << QString();
        pathsToTry << info.absolutePath();
    }
    else
    {
        QMessageBox::warning(0, "Unknown programming language", "Sorry, this programming language is not supported");
        return;
    }
    
    if (!fileName.isEmpty())
    {
        fileName = filePath(fileName, extensionsToTry, pathsToTry);
        if (!fileName.isEmpty())
            application()->activeMainWindow()->openUrl(fileName);
    }
    else
    {
        QMessageBox::warning(0, "Not import statement", "Probably this line is not import statement.");
    }
}

QString KatePluginOpenModule::cModuleToFileName(QString line, KTextEditor::Cursor)
{
    QRegExp regex("#include\\s+(?:<|\")((?:\\w+(?:\\\\|/))*\\w+(?:\\.\\w+)?)(?:>|\")");
    
    if (regex.indexIn(line) >= 0)
        return regex.cap(1);
    
    return QString();
}

QString KatePluginOpenModule::dModuleToFileName(QString line, KTextEditor::Cursor cursor)
{
    int column = cursor.column();
    QRegExp regex("import\\s+((?:\\w+\\.)*\\w+)(?:\\s*,\\s*((?:\\w+\\.)*\\w+))*");
    
    int indexIn = regex.indexIn(line);
    if (indexIn >= 0)
    {   
        for (int i=1; i<=regex.captureCount(); ++i)
        {
            
            QString cap = regex.cap(i);
            int pos = regex.pos(i);
            
            if (column >= pos && column <= pos+cap.size())
            {
                cap.replace(QChar('.'), QDir::separator());
                return cap;
            }
        }
    }
    
    return QString();
}

QString KatePluginOpenModule::filePath(QString fileBaseName, QStringList extensions, QStringList possiblePaths)
{
    QStringList filePaths;
    
    foreach(QString path, possiblePaths)
    {
        QDir dir(path);
        foreach(QString extension, extensions)
        {
            QString fileName = fileBaseName + (extension.isEmpty() ? QString() : (QChar('.') + extension));
            if (dir.exists(fileName))
                filePaths << dir.absoluteFilePath(fileName);
        }
    }
    
    if (filePaths.size() == 0)
    {
        QMessageBox::warning(0, "No such file", "Can't find corresponding file for this module.");
        return QString();
    }
    else if (filePaths.size() == 1)
    {
        return filePaths[0];
    }
    else
    {
        PathChooser* chooser = new PathChooser(filePaths, application()->activeMainWindow()->activeView());
        QString choice;
        if (chooser->exec() == QDialog::Accepted)
            choice = chooser->choice();
        delete chooser;
        return choice;
    }
}

//KatePluginOpenModuleView

KatePluginOpenModuleView::KatePluginOpenModuleView(Kate::MainWindow* mainWin)
    : Kate::PluginView(mainWin), Kate::XMLGUIClient(KatePluginOpenModuleFactory::componentData())
{
    KAction* act = actionCollection()->addAction("open_module");
    act->setText("Open Module");
    connect(act, SIGNAL(triggered(bool)), this, SIGNAL(openModuleRequested()));
    
    mainWindow()->guiFactory()->addClient(this);
}

KatePluginOpenModuleView::~KatePluginOpenModuleView()
{
    mainWindow()->guiFactory()->removeClient(this);
}

////KatePluginOpenModule

KatePluginOpenModuleConfigPage::KatePluginOpenModuleConfigPage(QWidget* parent, const char* name)
    : Kate::PluginConfigPage(parent, name)
{
    languageBoxes << new CLanguageBox << new DLanguageBox;
    
    KConfigGroup config(KGlobal::config(), "PluginOpenModule");
    QVBoxLayout* vbox = new QVBoxLayout;
    
    foreach(LanguageBox* languageBox, languageBoxes)
    {
        languageBox->addLines(config.readEntry(languageBox->key(), QStringList()));
        vbox->addWidget(languageBox);
    }

    setLayout(vbox);
}

void KatePluginOpenModuleConfigPage::apply()
{
    KConfigGroup config(KGlobal::config(), "PluginOpenModule");
    QStringList keys;
    foreach(LanguageBox* languageBox, languageBoxes)
    {
        QString key = languageBox->key();
        keys << key;
        config.writeEntry(key, languageBox->paths());
        config.writeEntry(key + "Extensions", languageBox->extensions());
    }
    config.writeEntry("OpenModules", keys);
    emit applied();
}

