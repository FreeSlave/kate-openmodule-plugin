#ifndef PATHCHOOSER_H
#define PATHCHOOSER_H

#include <QDialog>
#include <QStringList>

class QListWidgetItem;

class PathChooser : public QDialog
{
    Q_OBJECT
public:
    explicit PathChooser(QStringList lines, QWidget *parent = 0);
    QString choice() const {
        return chosen;
    }

signals:

public slots:
    void setChosen(QListWidgetItem* item);
private:
    QString chosen;
};

#endif // PATHCHOOSER_H
