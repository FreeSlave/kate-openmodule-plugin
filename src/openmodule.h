#ifndef OPENDMODULE_H
#define OPENDMODULE_H

#include <kate/plugin.h>
#include <kate/pluginconfigpageinterface.h>

#include <ktexteditor/cursor.h>

#include <KConfigGroup>
#include <KIcon>
#include <KLocale>
#include <KXMLGUIClient>

#include <QStringList>
#include <QMap>

namespace Kate
{
    class MainWindow;
    class Application;
}

class QListWidget;
class QPushButton;

class LanguageBox;

class KatePluginOpenModule : public Kate::Plugin, public Kate::PluginConfigPageInterface
{
    Q_OBJECT
    Q_INTERFACES(Kate::PluginConfigPageInterface)
public:
    //required by Kate::Plugin
    explicit KatePluginOpenModule(QObject* parent = 0, const QList<QVariant>& = QList<QVariant>());
    Kate::PluginView* createView (Kate::MainWindow *mainWin);
    
    //required by Kate::PluginConfigPageInterface
    uint configPages () const { return 1; }
    Kate::PluginConfigPage *configPage (uint , QWidget *w, const char *name = 0);
    QString configPageName(uint) const { return i18n("Open Module"); }
    QString configPageFullName(uint) const { return i18n("Open Module Configuration Page"); }
    QPixmap configPagePixmap (uint, int) const { return 0L; }
    KIcon configPageIcon (uint = 0) const { return KIcon(); }
    
    //user-defined slots
public slots:
    void openModuleSlot();
    void applyConfig();
    
    //user-defined methods
public:
    QString cModuleToFileName(QString line, KTextEditor::Cursor cursor);
    QString dModuleToFileName(QString line, KTextEditor::Cursor cursor);
    
    QString filePath(QString fileBaseName, QStringList extensions, QStringList possiblePaths);
    
private:
    QMap<QString, QStringList> extensionToPaths;
    QMap<QString, QStringList> extensionToExtensions;
};

class KatePluginOpenModuleView : public Kate::PluginView, public Kate::XMLGUIClient
{
    Q_OBJECT
public:
    explicit KatePluginOpenModuleView(Kate::MainWindow* mainWin);
    ~KatePluginOpenModuleView();
    
signals:
    void openModuleRequested();
};

class KatePluginOpenModuleConfigPage : public Kate::PluginConfigPage
{
    Q_OBJECT
public:
    KatePluginOpenModuleConfigPage(QWidget* parent = 0, const char* name = 0);
    
    virtual void apply();
    virtual void reset () { }
    virtual void defaults () { }
    
signals:
    void applied();
    
private:
    QList<LanguageBox*> languageBoxes;
};

#endif